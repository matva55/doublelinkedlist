
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <array>


using namespace std;

template<class T>
struct Node{
	T data;
	struct Node<T> *next;
	struct Node<T> *prev;
};

template<class T>
class doubleLinkedList{
	
	public:
		Node<T> *head_of_list;
		Node<T> *tail_of_list;
		int size;

		void addAtBeginning(T data){
			
			struct Node<T> *node_to_add;

			node_to_add = new(struct Node<T>);

			node_to_add->data = data;
			node_to_add->prev = NULL;
			node_to_add->next = head_of_list;

			head_of_list->prev = node_to_add;
			tail_of_list = head_of_list;
			head_of_list = node_to_add;

			size++;
		}

		void addAfterNode(T data, int position){
			
			struct Node<T> *node_to_add;
			struct Node<T> *current_node;

			if(position > size){
				cout << "ERROR: POSITION IS OUT OF BOUNDS." << endl;
				exit(EXIT_FAILURE);
			}

			node_to_add = new(struct Node<T>);
			node_to_add->data = data;

			current_node = head_of_list;
			int current_position = 0;

			while(current_position < position){
				if(current_node->next == NULL){
					break;
				}
				current_node = current_node->next;
				current_position++;

			}

			if(current_node->next == NULL){
				
				tail_of_list = node_to_add;
				node_to_add->prev = current_node;
				current_node->next = node_to_add;
				size++;

			}
			else if(current_node->prev == NULL){
				head_of_list = node_to_add;
				node_to_add->next = current_node;
				current_node->prev = node_to_add;
				size++;
			}
			else{
				node_to_add->prev = current_node->prev;
				node_to_add->next = current_node;
				current_node->prev->next = node_to_add;
				current_node->prev = node_to_add;
				size++;
			}

		}

		void deleteNode(T data){
			Node<T> *current_node;
			Node<T> *temporary_node;


			if(head_of_list->data == data){
				temporary_node = head_of_list;
				head_of_list = head_of_list->next;
				free(temporary_node);
				size--;
				return;
			}
			else if(tail_of_list->data == data){
				temporary_node = tail_of_list;
				tail_of_list = tail_of_list->prev;
				free(temporary_node);
				size--;
				return;
			}
			else{
				current_node = head_of_list;

				while(current_node != tail_of_list){

					if(current_node->data == data){
						temporary_node = current_node;
						current_node = current_node->next;
						current_node->prev = temporary_node->prev;
						temporary_node->prev->next = current_node;
						free(temporary_node);
						size--;
						return;
					}

					current_node = current_node->next;
				}

				cout << "ERROR: VALUE NOT FOUND!" << endl;
				return;
			}
		}

		void deleteList(){

			Node<T> *current_node;
			Node<T> *node_to_delete;

			current_node = head_of_list;

			while(current_node != tail_of_list){
				node_to_delete = current_node;
				current_node = current_node->next;

				deleteNode(node_to_delete->data);
			}


			deleteNode(current_node->data);
			head_of_list = NULL;
			tail_of_list = NULL;
		}

		Node<T>* getNode(T data){

			struct Node<T> *current_node;

			if(head_of_list == NULL){
				cout << "ERROR: NO DATA." << endl;
				return NULL;
			}

			current_node = head_of_list;

			while(current_node != NULL){

				if(current_node->data == data){
					return current_node;
				}

				current_node = current_node->next;
			}
			cout << "ERROR: No data with that value." << endl;
			return NULL;
		}

		void displayList(){
			int j = 0;
			Node<T> *node_to_display;

			node_to_display = head_of_list;

			if(head_of_list == NULL){
				cout << "ERROR: NO LIST TO DISPLAY." << endl;
				return;
			}

			while(j < size){

				cout << "The list is: " << node_to_display->data << endl;

				if(node_to_display->next == NULL){
					break;
				}

				node_to_display = node_to_display->next;
				j++;
			}
		}

		void reverse(){

			Node<T> *temporary_node;
			Node<T> *backwards_node;

			temporary_node = head_of_list;
			backwards_node = tail_of_list;

			head_of_list = backwards_node;
			tail_of_list = temporary_node;
			while(backwards_node->prev != NULL){

				temporary_node = backwards_node->prev;

				if(backwards_node->next == NULL){
					backwards_node->prev = NULL;
					backwards_node->next = temporary_node;
				}
				else{
					backwards_node->prev = backwards_node->next;
					backwards_node->next = temporary_node;
				}

				backwards_node = temporary_node;
			}


			backwards_node->prev = backwards_node->next;
			backwards_node->next = NULL;


		}

		int getSize(){
			return size;
		}

		T getData(Node<T>* node){
			return node->data;
		}

		doubleLinkedList(T data){

			struct Node<T> *first_node = new(struct Node<T>);

			first_node->data = data;
			first_node->prev = NULL;
			first_node->next = NULL;
			head_of_list = first_node;
			tail_of_list = first_node;
			size = 1;
		}
};

int main(){

	cout << "Beginning Tests." << endl;
	cout << "Testing constructor." << endl;


	doubleLinkedList<int> *testList = new doubleLinkedList<int>(10);

	struct Node<int> *temporaryNode;


	cout << "testList is..." << endl;

	testList->displayList();


	if(testList->head_of_list->next != NULL){
		cout << "Failure in setting next in constructor." << endl;
	}
	else if(testList->head_of_list->prev != NULL){
		cout << "Failure in setting the prev in constructor." << endl;
	}
	else if(testList->head_of_list->data != 10){
		cout << "Failure in setting the data in constructor." << endl;
	}
	else{
		cout << "Constructor works as intended." << endl;
	}

	cout << "Testing addAtBeginning function." << endl;

	testList->addAtBeginning(15);

	if(testList->head_of_list->next == NULL){
		cout << "Failure in setting the next node in the function." << endl;
	}
	else if(testList->head_of_list->prev != NULL){
		cout << "Failure in setting the prev node in the function." << endl;
	}
	else if(testList->head_of_list->data != 15){
		cout << "Failure in setting the data for the node." << endl;
	}
	else if(testList->head_of_list->next->prev != testList->head_of_list){
		cout << "Failure in adjusted the prev for former head_of_list." << endl;
	}
	else if(testList->tail_of_list->data != 10){
		cout << "Failure in setting the tail of the list." << endl;
	}
	else{
		cout << "addAtBeginning function is working." << endl;
	}

	cout << "Testing addAfterNode Function." << endl;

	testList->addAtBeginning(5);
	testList->addAtBeginning(20);
	testList->addAtBeginning(25);

	if(testList->getSize() == 1){
		cout << "Size is wrong." << endl;
	}

	cout << "The list size is: " << testList->getSize() << endl;

	Node<int> *current_node;

	testList->displayList();
	testList->addAfterNode(30, 3); //will put it in the THIRD position of list

	if(testList->getSize() != 6){
		cout << "Did not add Node correctly." << endl;
	}

	cout << "The list size is: " << testList->getSize() << endl;

	testList->displayList();

	testList->addAfterNode(50, 6);

	cout << "The list size is: " << testList->getSize() << endl;
	testList->displayList();

	
	testList->addAfterNode(70, 0);
	cout << "The list size is: " << testList->getSize() << endl;
	testList->displayList();


	Node<int> *test_node;
	test_node = testList->getNode(30);

	if(test_node->data != 30){
		cout << "Failure in acquiring correct node." << endl;
	}
	else if(test_node->next->data != 15){
		cout << "Failure in acquiring correct node." << endl;
	}
	else if(test_node->prev->data != 5){
		cout << "Failure in acquiring correct node." << endl;
	}
	cout << "The list size is: " << testList->getSize() << endl;

	testList->reverse();

	testList->displayList();

	cout << "The list size is: " << testList->getSize() << endl;

	testList->deleteNode(50);

	testList->displayList();

	cout << "The list size is: " << testList->getSize() << endl;

	testList->deleteNode(70);

	testList->displayList();

	cout << "The list size is: " << testList->getSize() << endl;

	testList->deleteNode(30);
	testList->deleteNode(30);

	testList->displayList();

	cout << "The list size is: " << testList->getSize() << endl;

	testList->deleteList();

	testList->displayList();

	cout << "Testing finished." << endl;
}